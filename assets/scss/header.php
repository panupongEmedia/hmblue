<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/style.min.css">
</head>
<body>

	<div id="header">
		<a id="logo" href="index.html" title="KING OF KING H.M. BLUES"></a>
		<div id="mainmenu-wrapper">
			<ul id="mainmenu">
				<li><a href="#">หน้าแรก</a></li>
				<li><a href="#">การแสดงการน้อมรำลึกที่ยิ่งใหญ่</a></li>
				<li><a href="#">WORLD RECORD SAXOPHONE</a></li>
				<li><a href="artist.html">ศิลปิน</a></li>
				<li><a href="#">สถานที่จัดงาน</a></li>
			</ul>
		</div>
	</div>