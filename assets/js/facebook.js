var fbAsyncInit = function() {
    FB.init({
      appId      : $('meta[name=facebook_app]').attr('content'),
      xfbml      : true,
      version    : 'v2.7'
    });

    FB.getLoginStatus(function(response){

    	if (response.status == 'connected'){
    		if ( $('#page-index').length ){
	    		// window.location = 'quiz.php';
	    	}
    	} else if (response.status === 'not_authorized') {
	    	
	    } else {

	    	if (! $('#page-index').length ){
	    		window.location = 'index.php';
	    	}
    	}
    });

    FB.AppEvents.logPageView();
};

(function(d, s, id){
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {return;}
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

var facebook = {
	'login': function () {
		FB.login(function(response) {

			if (response.status === 'connected') {
				FB.AppEvents.logEvent("Play Quiz");
				window.location = 'quiz.php';

	    	} else if (response.status === 'not_authorized') {
	    		FB.AppEvents.logEvent("Not Authorized");
	    	} else {
	    		FB.AppEvents.logEvent("Not Login");
	    	}

		}, {scope: 'public_profile, email'});
	}, 

	'logout': function(){
		FB.logout(function(response) {
			console.log(response);
		});
	},

	'getInfo': function() {
		FB.api('/me', 'GET', {fields: 'first_name,last_name,name,id, '}, function(response) {
			console.log(response.name, response.first_name, response.last_name, response.id);
			//ocument.getElementById('status').innerHTML = response.id;
			return response;
		});
	},

	'share': function(){

		// var obj = {
		// 	method: 'feed',
		// 	picture: pic_path,
		// 	link: 'http://demo.emedia.co.th/2016/fb_profile/',
		// 	name: 'กิน•มัน•ดะ',
		// 	description: 'คุณเป็นนักกินสายไหน?',
		// 	caption: 'คุณเป็นนักกินสายไหน?',
		// 	hashtag: '#kinmanda',
		// 	display: 'touch'
		// };

		FB.AppEvents.logEvent("Share to user feed");

		var obj = kinmanda_obj;

		FB.ui(obj, function(response) {
			// console.log(response);
			if (response && response.post_id) {
				FB.AppEvents.logEvent("Succeeded share to user feed");
			} else {
				FB.AppEvents.logEvent("Failed share to user feed");
			}
		});
	}
}