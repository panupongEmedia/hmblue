<?php $video = array(
	'IwhWcnJ3GXg'=>array('thumb'=>'assets/images/youtube/1.jpg', 'title'=>'youtube a'),
	'yqgZYQOP1PQ'=>array('thumb'=>'assets/images/youtube/2.jpg', 'title'=>'youtube b'),
	'abzNRTAyVt0'=>array('thumb'=>'assets/images/youtube/3.jpg', 'title'=>'youtube c'),
	'VLBJQgWkgog'=>array('thumb'=>'assets/images/youtube/4.jpg', 'title'=>'youtube d'),
	'M6P5qD_9b9o'=>array('thumb'=>'assets/images/youtube/5.jpg', 'title'=>'youtube e')
); ?>
<?php include 'header.php'; ?>
	<div id="page-world-record" class="bg-content">

		<div class="container">
			<h1 class="text-center title-border"><img src="assets/images/worldrecord.png" alt="World Record For Saxophone Ensemble"></h1>

			<div class="youtube">
				<div class="embled">
					<iframe width="100%" height="100%" src="https://www.youtube.com/embed/IwhWcnJ3GXg" frameborder="0" allowfullscreen></iframe>
				</div>
				<div class="youtube-list">
					<?php foreach ($video as $youtube_id=> $value): ?>
						<a class="item" style="background-image: url('<?php echo $value['thumb'] ?>')" data-youtube-id="<?php echo $youtube_id ?>" title="<?php echo $value['title'] ?>">
						</a>
					<?php endforeach ?>
				</div>
			</div>
		</div>

		<div class="ensemble">
			<div class="container">
				<h2 class="title-border text-center">ลงทะเบียนเพื่อร่วมเป่า Saxophone</h2>

				<div class="form-wrapper">
					<form action="action.php" method="post" id="form-ensemble">
						<div class="row">

							<!-- form left column -->
							<div class="col-lg-6">
								<b>ข้อมูลส่วนตัว</b>
								<div class="input-cols">
									<input type="text" name="firstname" placeholder="ชื่อ *" class="cols">
									<input type="text" name="lastname" placeholder="นามสกุล *" class="cols">
									<div class="clearfix"></div>
								</div>
								<input type="text" name="id" placeholder="หมายเลขบัตรประชาชน *">
								<input type="text" name="email" placeholder="อีเมล์ *">
								<input type="text" name="phonenum" placeholder="เบอร์โทรศัพท์ติดต่อ *">
								<div class="radio-wrapper">
									<span>ไซต์เสื้อ</span>
									<label><input type="radio" name="size" value="s" class="radio"><i>S</i></label>
									<label><input type="radio" name="size" value="m" class="radio"><i>M</i></label>
									<label><input type="radio" name="size" value="l" class="radio"><i>L</i></label>
									<label><input type="radio" name="size" value="xl" class="radio"><i>XL</i></label>
									<div class="clearfix"></div>
								</div>
								<textarea name="" id="" cols="30" rows="10" placeholder="ข้อมูลเพิ่มเติม"></textarea>
							</div>

							<!-- form right column -->
							<div class="col-lg-6">
								<b>บุคคลอ้างอิง</b>
								<div class="input-cols">
									<input type="text" name="fname2" placeholder="ชื่อ" class="cols">
									<input type="text" name="lname2" placeholder="นามสกุล" class="cols">
									<div class="clearfix"></div>
								</div>
								<input type="text" name="" placeholder="มีความสัมพันธ์เป็น">
								<input type="text" name="phonenum2" placeholder="เบอร์โทรศัพท์ติดต่อ">
							</div>
						</div>
						<div class="form-button text-center">
							<button type="submit">ลงทะเบียน</button>
						</div>
					</form>
				</div>
			</div>
		</div>

		<div class="howtoplay">
			<div class="container">
				<h2 class="text-center title-border">How to play <strong>“Oh I Say”</strong></h2>
				<div class="embled">
					<iframe width="100%" height="100%" src="https://www.youtube.com/embed/A8g0R-iswJQ" frameborder="0" allowfullscreen></iframe>
				</div>
				<div class="open">
					<ul>
						<li><a href="assets/images/ohisay.jpg" class="swipebox"><img src="assets/images/i1.jpg" alt=""> ดูคอร์ดเพลง Oh I Say</a></li>
						<!-- <li><a href="#"><img src="assets/images/i2.jpg" alt=""> พิมพ์คอร์ดเพลง Oh I Say</a></li> -->
					</ul>
				</div>
			</div>
		</div>

		<div id="success">
			
		</div>
	</div>
<?php include 'footer.php'; ?>