// var gulp = require('gulp');
// var cleanCSS = require('gulp-clean-css');

// gulp.task('minify-css', function() {
//   return gulp.src('assets/css/*.css')
//     .pipe(cleanCSS({compatibility: 'ie8'}))
//     .pipe(gulp.dest('assets/dist'));
// });


var gulp = require('gulp');
var minify = require('gulp-minify-css');
var rename = require('gulp-rename');

gulp.task('minify', function () {
    gulp.src('assets/css/*.css')
        .pipe(minify({keepBreaks: true}))
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest('assets/css/'))
    ;
});