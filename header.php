<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="Cache-control" content="public">
	<title>H.M. BLUES Tribute</title>
	<link rel="stylesheet" href="assets/css/colorbox.css">
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/style.min.css">
	<link rel="stylesheet" href="assets/css/slick.css">
	<link rel="stylesheet" href="assets/css/swipebox.css">
	<link rel="icon" href="assets/images/favicon.ico" type="image/x-icon" />
	
</head>
<body <?php echo isset($pagehome) ? 'class="bg-home"' : '' ?>>

	<div id="header">
		<div class="container"></div>

		<a id="logo" href="index.php" title="KING OF KING H.M. BLUES"></a>

		<?php if (isset($pagehome)): ?>
		<a href="world-record-saxophone.php" id="register_header" class="visible-lg">
			<img src="./assets/images/register.png" alt="">
		</a>
		<?php endif ?>

		<div id="mainmenu-wrapper">
			<ul id="mainmenu">
				<li><a href="index.php">หน้าแรก</a></li>
				<li><a href="memorial.php">การแสดงการน้อมรำลึกที่ยิ่งใหญ่</a></li>
				<li><a href="world-record-saxophone.php">WORLD RECORD SAXOPHONE</a></li>
				<li><a href="artist.php">ศิลปิน</a></li>
				<li><a href="location.php">สถานที่จัดงาน</a></li>
			</ul>
		</div>
	</div>