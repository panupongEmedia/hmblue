	

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/style.min.css">
</head>
<body>
	<div id="page-thankyou">
		<div class="container">
			<h1>ลงทะเบียนสำเร็จแล้ว</h1>
			<h2>รายละเอียดการลงทะเบียนของคุณ</h2>

			<div class="table-wrapper">
				<table cellspacing="0" cellpadding="0" width="50%">
					<tr>
						<td colspan="2"><b>ข้อมูลส่วนตัว</b></td>
						<td colspan="2"><b>ข้อมูลบุคคลอ้างอิง</b></td>
					</tr>
					<tr>
						<th>ชื่อ :</th>
						<th>นามสกุล :</th>
						<th>ชื่อ :</th>
						<th>นามสกุล :</th>
					</tr>
					<tr>
						<td>สมชาย</td>
						<td>จิตอาสา</td>
						<td>-</td>
						<td>-</td>
					</tr>
					<tr>
						<th colspan="2">หมายเลขบัตรประชาชน</th>
						<th colspan="2">มีความสัมพันธ์เป็น</th>
					</tr>
					<tr>
						<td colspan="2">1 1223 1682 5xxx</td>
						<td colspan="2">-</td>
					</tr>
					<tr>
						<th colspan="2">อีเมล์</th>
						<th colspan="2">เบอร์โทรศัพท์ติดต่อ</th>
					</tr>
					<tr>
						<td colspan="2">somchai@gmail.com</td>
						<td colspan="2">-</td>
					</tr>
					<tr>
						<th colspan="4">ไซต์เสื้อ</th>
					</tr>
					<tr>
						<td colspan="2">M</td>
						<td colspan="2">
							<p class="regiscode">REGIS CODE: 5DR8C</p>
						</td>
					</tr>
					<tr>
						<th colspan="4">ข้อมูลเพิ่มเติม</th>
					</tr>
					<tr>
						<td colspan="4">-</td>
					</tr>
					<!-- <tr>
						<td colspan="4">
							
						</td>
					</tr> -->
				</table>		
			</div>
		
		</div>
	</div>
</body>
</html>
