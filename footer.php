	<div id="footer-social">
		<div class="container text-center">
			<ul>
				<li>
					<a href="#">
						<span class="img"><img src="assets/images/fb.png" alt=""></span>
						<span class="caption">
							<i>HM Blues</i>
							<b>Facebook Fanpage</b>
						</span>						
					</a>
				</li>
				<li>
					<a href="#">
						<span class="img"><img src="assets/images/yt.png" alt=""></span>
						<span class="caption">
							<i>HM_Blues</i>
							<b>Youtube Channel</b>
						</span>						
					</a>
				</li>
				<li>
					<a href="#">
						<span class="img"><img src="assets/images/call.png" alt=""></span>
						<span class="caption">
							<i>02 788 8888</i>
							<b>Call Center</b>
						</span>						
					</a>
				</li>
			</ul>
		</div>
	</div>
	<div id="footer">
		<div class="container text-center">
			<div class="sponsor">
				<ul>
					<li><a href="#"><img src="./assets/images/sponsor/1.png" alt=""></a></li>
					<li><a href="#"><img src="./assets/images/sponsor/2.png" alt=""></a></li>
					<li><a href="#"><img src="./assets/images/sponsor/3.png" alt=""></a></li>
					<li><a href="#"><img src="./assets/images/sponsor/4.png" alt=""></a></li>
				</ul>
			</div>
			<p>© Copyright 2016 <span>HM Blues</span>, All Rights Reserved.</p>	
		</div>
	</div>
	<script src="assets/js/jquery-2.1.1.js"></script>
	<script src="assets/js/jquery.validate.min.js"></script>
	<script src="assets/js/jquery.swipebox.min.js"></script>
	<script src="assets/js/slick.min.js"></script>
	<script src="assets/js/jquery.colorbox-min.js"></script>
	<script src="assets/js/jquery.easing.1.3.js"></script>
	<script>
		$(document).ready(function(){

			$('.youtube-list').slick({
				slidesToShow: 3,
				slidesToScroll: 1
			});

			$('.youtube-list .item').click(function(){
				$('.youtube .embled').html('<iframe width="100%" height="100%" src="https://www.youtube.com/embed/'+ $(this).data('youtube-id') +'?autoplay=true" frameborder="0" allowfullscreen></iframe>');
			});

			function messageSuccess(response){
				// console.log(response.register_code);
				$.colorbox({href:"thankyou.php"});
			}

			$('#form-ensemble').validate({
				// onclick: false, 
    			// onkeyup: false,
				rules: {
					firstname: "required",
					lastname: "required",
					email: {
						required: true,
						email: true
					},
					id : {
						required: true,
						maxlength: 13,
						digits: true
					},
					phonenum: {
						required: true,
						digits: true,
						minlength:10, 
						maxlength:15
						// phoneUS: true,
						// matches:"[0-9]+",minlength:10, maxlength:15
					},
					phonenum2: {
						// required: true,
						digits: true,
						minlength:10, 
						maxlength:15
						// phoneUS: true,
						// matches:"[0-9]+",minlength:10, maxlength:15
					},
					size: {
						required: true
					}

				},

				errorPlacement: function (error, element) {
		            // console.log(element);
		            // console.log(error);
		        },

			    submitHandler: function(form) {
			        $.ajax({
			            url: form.action,
			            type: form.method,
			            data: $(form).serialize(),
			            dataType: "json",
			            success: function(response) {
			                messageSuccess(response);
			            }            
			        });
    			}
			});

			$('#page-home .event-wrapper').slick();

			$( '.swipebox' ).swipebox();


			// function floating(){
			// 	if ($(document).scrollTop() > menurPosition){
			// 		var a = $(window).scrollTop() -  ($("#header").innerHeight() + $("#menu").innerHeight());
			// 		$fl_rmenu.stop().animate({top: a }, $float_speed, $float_easing);
			// 	} else {
			// 		$fl_rmenu.stop().animate({top: 0 }, $float_speed, $float_easing);
			// 	}
			// }

			var $float_speed=1500; //milliseconds
			var $float_easing="easeOutQuint";
			$(document).scroll(function(){
				$('#register_header').stop().animate({top: $(window).scrollTop() }, $float_speed, $float_easing);
			});
		});
	</script>
</body>
</html>