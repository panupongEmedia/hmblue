<?php include 'mail_header.php'; ?>

<table cellspacing="0" cellpadding="0" width="100%">
	<tr>
		<td colspan="2" style="font-family: tahoma; color:#030c22; font-size: 20px; font-weight: bold; padding: 10px 0;">ข้อมูลส่วนตัว</td>
		<td colspan="2" style="font-family: tahoma; color:#030c22; font-size: 20px; font-weight: bold; padding: 10px 0;">ข้อมูลบุคคลอ้างอิง</td>
	</tr>
	<tr>
		<td style="font-family: tahoma; color:#030c22; font-size: 14px; font-weight: bold; padding: 10px 0 0;">ชื่อ :</td>
		<td style="font-family: tahoma; color:#030c22; font-size: 14px; font-weight: bold; padding: 10px 0 0;">นามสกุล :</td>
		<td style="font-family: tahoma; color:#030c22; font-size: 14px; font-weight: bold; padding: 10px 0 0;">ชื่อ :</td>
		<td style="font-family: tahoma; color:#030c22; font-size: 14px; font-weight: bold; padding: 10px 0 0;">นามสกุล :</td>
	</tr>
	<tr>
		<td style="font-family: tahoma; color:#030c22; font-size: 18px; padding: 10px 0 20px;">สมชาย</td>
		<td style="font-family: tahoma; color:#030c22; font-size: 18px; padding: 10px 0 20px;">จิตอาสา</td>
		<td style="font-family: tahoma; color:#030c22; font-size: 18px; padding: 10px 0 20px;">-</td>
		<td style="font-family: tahoma; color:#030c22; font-size: 18px; padding: 10px 0 20px;">-</td>
	</tr>
	<tr>
		<td colspan="2" style="font-family: tahoma; color:#030c22; font-size: 14px; font-weight: bold; padding: 10px 0 0;">หมายเลขบัตรประชาชน</td>
		<td colspan="2" style="font-family: tahoma; color:#030c22; font-size: 14px; font-weight: bold; padding: 10px 0 0;">มีความสัมพันธ์เป็น</td>
	</tr>
	<tr>
		<td colspan="2" style="font-family: tahoma; color:#030c22; font-size: 18px; padding: 10px 0 20px;">1 1223 1682 5xxx</td>
		<td colspan="2" style="font-family: tahoma; color:#030c22; font-size: 18px; padding: 10px 0 20px;">-</td>
	</tr>
	<tr>
		<td colspan="2" style="font-family: tahoma; color:#030c22; font-size: 14px; font-weight: bold; padding: 10px 0 0;">อีเมล์</td>
		<td colspan="2" style="font-family: tahoma; color:#030c22; font-size: 14px; font-weight: bold; padding: 10px 0 0;">เบอร์โทรศัพท์ติดต่อ</td>
	</tr>
	<tr>
		<td colspan="2" style="font-family: tahoma; color:#030c22; font-size: 18px; padding: 10px 0 20px;">somchai@gmail.com</td>
		<td colspan="2" style="font-family: tahoma; color:#030c22; font-size: 18px; padding: 10px 0 20px;">-</td>
	</tr>
	<tr>
		<td colspan="4" style="font-family: tahoma; color:#030c22; font-size: 14px; font-weight: bold; padding: 10px 0 0;">ไซต์เสื้อ</td>
	</tr>
	<tr>
		<td colspan="4" style="font-family: tahoma; color:#030c22; font-size: 18px; padding: 10px 0 20px;">M</td>
	</tr>
	<tr>
		<td colspan="4" style="font-family: tahoma; color:#030c22; font-size: 14px; font-weight: bold; padding: 10px 0 0;">ข้อมูลเพิ่มเติม</td>
	</tr>
	<tr>
		<td colspan="4" style="font-family: tahoma; color:#030c22; font-size: 18px; padding: 10px 0 20px;">-</td>
	</tr>
	<tr>
		<td colspan="4">
			<p style="padding: 50px; border: solid 1px #ccc; text-align: center; font-size: 30px;">REGIS CODE: 5DR8C</p>		
		</td>
	</tr>
</table>

<?php include 'mail_footer.php'; ?>