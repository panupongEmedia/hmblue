<?php include 'header.php'; ?>


	<div id="page-artist" class="bg-content">
		<h1 class="text-center title-border">ศิลปินที่ร่วมงาน</h1>
		<div class="artist-wrapper text-center">
			<ul>
				<li><a href="#" title="กิตตินันท์ ชินสำราญ"><img src="assets/images/artist/1.jpg" alt=""></a></li>
				<li><a href="#" title="กิตตินันท์ ชินสำราญ"><img src="assets/images/artist/2.jpg" alt=""></a></li>
				<li><a href="#" title="กิตตินันท์ ชินสำราญ"><img src="assets/images/artist/3.jpg" alt=""></a></li>
				<li><a href="#" title="กิตตินันท์ ชินสำราญ"><img src="assets/images/artist/4.jpg" alt=""></a></li>
				<li><a href="#" title="กิตตินันท์ ชินสำราญ"><img src="assets/images/artist/5.jpg" alt=""></a></li>
				<li><a href="#" title="กิตตินันท์ ชินสำราญ"><img src="assets/images/artist/6.jpg" alt=""></a></li>
				<li><a href="#" title="กิตตินันท์ ชินสำราญ"><img src="assets/images/artist/7.jpg" alt=""></a></li>
				<li><a href="#" title="กิตตินันท์ ชินสำราญ"><img src="assets/images/artist/8.jpg" alt=""></a></li>
				<li><a href="#" title="กิตตินันท์ ชินสำราญ"><img src="assets/images/artist/9.jpg" alt=""></a></li>
				<li><a href="#" title="กิตตินันท์ ชินสำราญ"><img src="assets/images/artist/10.jpg" alt=""></a></li>
				<li><a href="#" title="กิตตินันท์ ชินสำราญ"><img src="assets/images/artist/11.jpg" alt=""></a></li>
				<li><a href="#" title="กิตตินันท์ ชินสำราญ"><img src="assets/images/artist/12.jpg" alt=""></a></li>
				<li><a href="#" title="กิตตินันท์ ชินสำราญ"><img src="assets/images/artist/13.jpg" alt=""></a></li>
				<li><a href="#" title="กิตตินันท์ ชินสำราญ"><img src="assets/images/artist/14.jpg" alt=""></a></li>
				<li><a href="#" title="กิตตินันท์ ชินสำราญ"><img src="assets/images/artist/15.jpg" alt=""></a></li>
				<li><a href="#" title="กิตตินันท์ ชินสำราญ"><img src="assets/images/artist/16.jpg" alt=""></a></li>
				<li><a href="#" title="กิตตินันท์ ชินสำราญ"><img src="assets/images/artist/17.jpg" alt=""></a></li>
				<li><a href="#" title="กิตตินันท์ ชินสำราญ"><img src="assets/images/artist/18.jpg" alt=""></a></li>
				<li><a href="#" title="กิตตินันท์ ชินสำราญ"><img src="assets/images/artist/19.jpg" alt=""></a></li>
				<li><a href="#" title="กิตตินันท์ ชินสำราญ"><img src="assets/images/artist/20.jpg" alt=""></a></li>
				<li><a href="#" title="กิตตินันท์ ชินสำราญ"><img src="assets/images/artist/21.jpg" alt=""></a></li>
				<li><a href="#" title="กิตตินันท์ ชินสำราญ"><img src="assets/images/artist/22.jpg" alt=""></a></li>
				<li><a href="#" title="กิตตินันท์ ชินสำราญ"><img src="assets/images/artist/23.jpg" alt=""></a></li>
				<li><a href="#" title="กิตตินันท์ ชินสำราญ"><img src="assets/images/artist/24.jpg" alt=""></a></li>
			</ul>
			<div class="clearfix"></div>
		</div>
	</div>


<?php include 'footer.php'; ?>