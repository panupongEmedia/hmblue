<?php include 'header.php'; ?>
	<div id="page-memorial" class="bg-content">
		<div class="container">
			<h1 class="text-center title-border">การแสดงการน้อมรำลึกที่ยิ่งใหญ่</h1>
			<div class="event-wrapper text-center">
				<div class="row">
					<div class="col-lg-6"><img src="assets/images/memorial/1.png" alt=""></div>
					<div class="col-lg-6">
						<b class="event-title">SKY MAGIC DRONE</b>
						การแสดงการแปลอักษรบนฟ้าจาก Drone เพื่อน้อมรำลึกถึง<br class="visible-lg">
						พระบาทสมเด็จพระปรมินทรมหาภูมิพลอดุลยเดช<br class="visible-lg">
						พระผู้ทรงสถิต ณ ฟากฟ้า
					</div>
				</div>
				<div class="row">
					<div class="col-lg-6">
						<b class="event-title">WORLD RECORD <br>FOR SAXOPHONE ENSEMBLE</b>
						บรรเลงเพลง Oh I Say เพื่อทำลายสถิติกินเนสฯ และ<br class="visible-lg">
						แสดงพลังแห่งความจงรักภักดีถวายแด่<br class="visible-lg">
						พระบาทสมเด็จพระเจ้าอยู่หัวให้โลกประจักษ์
					</div>
					<div class="col-lg-6"><img src="assets/images/memorial/2.png" alt=""></div>
				</div>
				<div class="row">
					<div class="col-lg-6"><img src="assets/images/memorial/3.png" alt=""></div>
					<div class="col-lg-6">
						<b class="event-title">SAND ANIMATION</b>
						การเขียนทรายภาพพระราชกรณียกิจ<br class="visible-lg">
						โดยตีความจากบทเพลงพระราชนิพนธ์<br class="visible-lg">
						โดยศิลปินเขียนทรายชาวยูเครน ที่มีชื่อเสียงระดับโลก<br class="visible-lg">
						Kseniya Simonova
					</div>
				</div>
				<div class="row">
					<div class="col-lg-6">
						<b class="event-title">ROYAL MARCH</b>
						การแปรขบวนทหารม้าราชวัลลภ ที่ยิ่งใหญ่และสง่างาม<br class="visible-lg">
						ประกอบเพลงมาร์ชราชวัลลภ
					</div>
					<div class="col-lg-6"><img src="assets/images/memorial/4.png" alt=""></div>
				</div>
				<div class="row">
					<div class="col-lg-6"><img src="assets/images/memorial/5.png" alt=""></div>
					<div class="col-lg-6">
						<b class="event-title">CHRONICLES OF THE LAND (นิทานแผ่นดิน)</b>
						ภาพยนต์อนิเมชั่น 3 มิติชุด “นิทานแผ่นดิน”<br class="visible-lg">
						ประกอบเพลง “นิทานแผ่นดิน” ที่ประพันธ์ขึ้นใหม่<br class="visible-lg">
						โดย พงศ์พรหม สนิทวงศ์ ณ อยุธยา
					</div>
				</div>
				<div class="row">
					<div class="col-lg-6">
						<b class="event-title">3D PROJECTION MAPPING</b>
						การฉายภาพ 3D Mapping พระราชกรณียกิจที่สำคัญ<br class="visible-lg">
						ที่สอดคล้องกับห้วงเวลา ที่ทรงพระราชนิพนธ์บทเพลงนั้น<br class="visible-lg"> เพื่อให้ทุกคนร่วมน้อมรำลึกในพระมหากรุณาธิคุณ
					</div>
					<div class="col-lg-6"><img src="assets/images/memorial/6.png" alt=""></div>
				</div>
				<div class="row">
					<div class="col-lg-6"><img src="assets/images/memorial/7.png" alt=""></div>
					<div class="col-lg-6">
						<b class="event-title">รถศูนย์ข้อมูลดนตรี</b>
						รถศูนย์ข้อมูลดนตรี หอสมุดคนตรี<br class="visible-lg">
						พระบาทสมเด็จพระเจ้าอยู่หัว รัชกาลที่ ๙
					</div>
				</div>
				<div class="row">
					<div class="col-lg-6">
						<b class="event-title">สร้างสรรค์ สามัคคี เยาวชนไทย</b>
						การเปิดโอกาศรวมพลังความสามัคคี<br class="visible-lg">
						ของนักเรียน นิสิต และนักศึกษา<br class="visible-lg">
						ในการแสดงบทเพลงพระราชนิพนธ์ของพ่อ <br class="visible-lg">
						ด้วยความสร้างสรรค์
						
					</div>
					<div class="col-lg-6"><img src="assets/images/memorial/8.png" alt=""></div>
				</div>
			</div>
		</div>
	</div>
<?php include 'footer.php'; ?>