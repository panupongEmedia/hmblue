var gulp = require('gulp');
 
var sass = require('gulp-sass');
var concat    = require('gulp-concat');
var minifyCSS = require('gulp-minify-css');
var rename    = require('gulp-rename');
var imagemin = require('gulp-imagemin');
var pngquant = require('imagemin-pngquant');
var browserSync = require('browser-sync');

var uglify = require('gulp-uglify');
// var fileinclude = require('gulp-file-include');
var connect = require('gulp-connect-php');


function swallowError (error) {

  // If you want details of the error in the console
  console.log(error.toString());

  this.emit('end');
}

gulp.task('connect-sync', function() {
  connect.server({}, function (){
    browserSync({
      proxy: '127.0.0.1:80/hmblue'
    });
  });
 
  gulp.watch('**/*.php').on('change', function () {
    browserSync.reload();
  });
});

gulp.task('compress', function() {
  return gulp.src('assets/js/global.js')
    .pipe(uglify())
    .pipe(rename('global.min.js'))
    .pipe(gulp.dest('assets/js'));
});

// gulp.task('sass', function () {
//     gulp.src('assets/scss/global.scss')
//         .pipe(sass())
//         .on('error', swallowError)
//         .pipe(gulp.dest('assets/css'))
//         .pipe(concat('style.css'))
//         .pipe(gulp.dest('./'))
//         .pipe(minifyCSS())
//         .pipe(rename('style.min.css'))
//         .pipe(gulp.dest('assets/css'));
// });

gulp.task('sass', function () {
    gulp.src('assets/scss/global.scss')
        .pipe(sass())
        .on('error', swallowError)
        .pipe(gulp.dest('assets/css'))
        // .pipe(concat('style.min.css'))
        // .pipe(gulp.dest('assets/css'))
        .pipe(minifyCSS())
        .pipe(rename('style.min.css'))
        .pipe(gulp.dest('assets/css'));
});

gulp.task('sass_m', function () {
    gulp.src('assets/m/scss/global.scss')
        .pipe(sass())
        .on('error', swallowError)
        .pipe(gulp.dest('assets/m/css'))
        .pipe(concat('style.css'))
        .pipe(gulp.dest('./'))
        .pipe(minifyCSS())
        .pipe(rename('style.min.css'))
        .pipe(gulp.dest('assets/m/css'));
});
 
gulp.task('browser-sync', function() {
    browserSync({
        server: {
            baseDir: "./"
        }
    });
});

gulp.task('min-images', function () {
    return gulp.src('assets/images/*')
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()]
        }))
        .pipe(gulp.dest('assets/images/'));
});

gulp.task('default', ['connect-sync','sass'], function() {
    gulp.watch(['**/*.html'], browserSync.reload);
    gulp.watch("assets/js/**/*.js", ['compress']);
    gulp.watch("assets/scss/**/*.scss", ['sass']);
    //gulp.watch("assets/scss/**/*.scss", browserSync.reload);
    gulp.watch("assets/m/scss/**/*.scss", ['sass_m']);
});

// gulp.task('default', ['sass'], function() {
//     gulp.watch("assets/js/**/*.js", ['compress']);
//     gulp.watch("assets/scss/**/*.scss", ['sass']);
//     gulp.watch("assets/m/scss/**/*.scss", ['sass_m']);
// });


gulp.task('fileinclude', function() {
  gulp.src(['index.html'])
    .pipe(fileinclude({
      prefix: '@@',
      basepath: '@file'
    }))
    .pipe(gulp.dest('./'));
});
