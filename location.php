<?php include 'header.php'; ?>
	<div id="page-location" class="bg-content">
		<div class="container">
			<h1 class="text-center title-border">สถานที่จัดงาน</h1>
			<div class="location text-center">
				<div class="line-1">สนามกีฬากองทัพบก</div>
				<div class="line-2">ถนนวิภาวดีรังสิต แขวงสามเสนใน เขตพญาไท กรุงเทพมหานคร 10400</div>
				<div class="line-3">วันเสาร์ที่ ๔ กุมภาพันธ์ พุทธศักราช ๒๕๖๐ <br>เวลา ๒๐.๐๐ - ๒๓.๓๐ น.</div>
			</div>
			<div id="map"></div>
		</div>
	</div>
<?php include 'footer.php'; ?>


<script>
	function initMap() {
		var myLatLng = {lat: 13.782623, lng: 100.556853}; 

		// Create a map object and specify the DOM element for display.
		var map = new google.maps.Map(document.getElementById('map'), {
			center: myLatLng,
			scrollwheel: false,
			zoom: 15
		});

		// Create a marker and set its position.
		var marker = new google.maps.Marker({
			map: map,
			position: myLatLng,
			title: 'Hello World!'
		});
	}
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAbyJpeHZiuwPawFe6p5hBgjL0Oz1JTi8s&callback=initMap" async defer></script>